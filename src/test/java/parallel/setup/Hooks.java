package parallel.setup;

import core.driver.DriverSetup;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Hooks {
    public static WebDriver driver;
    private static String mainUrl;
    private static String browser;
    private static String env;

    public static String getMainUrl() { return mainUrl; }
    public static String getBrowser() { return browser; }
    public static String getEnv() { return env; }

    @Before
    public void setup(){
        setUpProperties();
        setUpEnv(env);
        driver = new DriverSetup().web(browser);
        driver.manage().window().maximize();
    }

    @After
    public void shutDown(){
        if(driver!=null){
            driver.quit();
        }
    }

    public void setUpProperties(){
        InputStream is = getClass().getClassLoader()
                .getResourceAsStream("my.properties");
        Properties properties = new Properties();
        try {
            properties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        browser = properties.getProperty("browser");
        env = properties.getProperty("env");
    }

    public void setUpEnv(String env){
        Map<String, String> urls = new HashMap<>();
        urls.put("stg_ef", "http://stg-ef.labpetz.com.br/");
        urls.put("prd", "http://www.petz.com.br");

        mainUrl = urls.get(env);
    }
}
