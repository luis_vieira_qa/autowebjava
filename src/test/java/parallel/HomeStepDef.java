package parallel;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import pages.home.Home;


public class HomeStepDef {
    private Home home = new Home();

    @Given("I access the browser page")
    public void accessPage() {
        home.openPage();
    }

    @When("I search for {string}")
    public void searchFor(String string) {
        home.fillSearch(string);
        home.makeSearch();
    }
}
