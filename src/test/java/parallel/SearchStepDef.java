package parallel;

import io.cucumber.java.en.Then;
import pages.search.Search;

import static org.junit.Assert.assertTrue;


public class SearchStepDef {
    private Search search = new Search();

    @Then("result links are displayed")
    public void resultLinksAreDisplayed() {
        assertTrue("busca efetuada com sucesso", search.isSearchReturningResults());
    }
}
