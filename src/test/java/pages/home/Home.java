package pages.home;

import core.elements.core.ElActions;
import parallel.setup.Hooks;

public class Home {
    private ElActions act;

    public Home(){
        act = new ElActions(this.getClass().getName());
    }

    public void openPage(){
        act.getExtras().goTo(Hooks.getMainUrl());
    }

    public void fillSearch(String searchTerm) {
        act.getKeys().sendKeysEl("search_bar", searchTerm);
    }

    public void makeSearch(){
        act.getClicks().clickEl("search_button");
    }


}
