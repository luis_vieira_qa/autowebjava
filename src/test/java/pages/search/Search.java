package pages.search;

import core.elements.core.ElActions;

public class Search {
    private ElActions act;

    public Search(){
        act = new ElActions(this.getClass().getName());
    }

    public boolean isSearchReturningResults(){
        return act.getChecks().isDisplayed("results_imgs", 30);
    }


}
