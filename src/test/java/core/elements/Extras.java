package core.elements;

import core.elements.core.Finder;
import core.elements.reader.elementObject.YmlElementObject;

public class Extras extends Finder {

    public Extras(YmlElementObject elementList, String pageEl){
        this.elementList = elementList;
        this.pageEl = pageEl;
    }
    public void scrollToElement(String element){
        getJsExecutor().executeScript("arguments[0].scrollIntoView()", findEl(element));
    }

    public void goTo(String url){
        driver.get(url);
    }

    public void switchToNewWindow() {
        Object[] windows = driver.getWindowHandles().toArray();
        driver.switchTo().window(windows[1].toString());
    }

    public void closeAndSwitchBackDefaultWindow() {
        Object[] window = driver.getWindowHandles().toArray();
        driver.close();
        driver.switchTo().window(window[0].toString());
    }


}
