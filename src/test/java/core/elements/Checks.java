package core.elements;

import core.elements.core.Finder;
import core.elements.reader.elementObject.YmlElementObject;

public class Checks extends Finder {

    public Checks(YmlElementObject elementList, String pageEl){
        this.elementList = elementList;
        this.pageEl = pageEl;
    }

    public boolean isDisplayed(String element, int timeout){
        return checkDisplayed(element, timeout);
    }
}
