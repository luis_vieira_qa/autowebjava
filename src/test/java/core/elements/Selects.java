package core.elements;

import core.elements.core.Finder;
import core.elements.reader.elementObject.YmlElementObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class Selects extends Finder {

    public Selects(YmlElementObject elementList, String pageEl){
        this.elementList = elementList;
        this.pageEl = pageEl;
    }

    public void selectByText(String element, String text){
        Select dropdown = new Select(findEl(element));
        dropdown.selectByVisibleText(text);
    }

    public void selectByIndex(String element, int index){
        Select dropdown = new Select(findEl(element));
        dropdown.selectByIndex(index);
    }

    public void selectRandomOption(String element){
        Select dropdown = new Select(findEl(element));
        List<WebElement> getList = dropdown.getOptions();
        dropdown.selectByIndex(randomizer(0,getList.size()));
    }

}
