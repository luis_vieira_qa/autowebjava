package core.elements;

import core.elements.core.Finder;
import core.elements.reader.elementObject.YmlElementObject;
import org.openqa.selenium.StaleElementReferenceException;

public class Keys extends Finder {

    public Keys(YmlElementObject elementList, String pageEl){
        this.elementList = elementList;
        this.pageEl = pageEl;
    }

    public void sendKeysEl(String element, String text){
        try {
            findEl(element).sendKeys(text);
        } catch (StaleElementReferenceException e){
            findEl(element).sendKeys(text);
        }
    }
}
