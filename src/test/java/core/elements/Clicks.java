package core.elements;

import core.elements.core.Finder;
import core.elements.reader.elementObject.YmlElementObject;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Clicks extends Finder {

    public Clicks(YmlElementObject elementList, String pageEl){
        this.elementList = elementList;
        this.pageEl = pageEl;
    }

    public void clickEl(String element){
        try {
            waitClickable(findEl(element)).click();
        } catch (StaleElementReferenceException | ElementClickInterceptedException e){
            emergencySleep();
            waitClickable(findEl(element)).click();
        }
    }

    public void moveAndClick(String element){
        getActions().moveToElement(waitClickable(findEl(element))).click().perform();
    }

    public void clickRandomEl(String element){
        List<WebElement> list = findList(element);
        waitClickable(list.get(randomizer(0,list.size()))).click();
    }
}
