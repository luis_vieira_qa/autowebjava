package core.elements.core;

import core.elements.*;
import core.elements.reader.ElementReader;
import core.elements.reader.elementObject.YmlElementObject;

public class ElActions {
    private Clicks clicks;
    private Checks checks;
    private Keys keys;
    private Extras extras;
    private Selects selects;

    public ElActions(String pageEl){

        YmlElementObject elementList = new ElementReader().elementObject(pageEl);

        clicks = new Clicks(elementList, pageEl);
        checks = new Checks(elementList, pageEl);
        keys = new Keys(elementList,pageEl);
        extras = new Extras(elementList,pageEl);
        selects = new Selects(elementList,pageEl);
    }

    public Clicks getClicks(){
        return this.clicks;
    }

    public Checks getChecks(){
        return this.checks;
    }

    public Keys getKeys(){
        return this.keys;
    }

    public Extras getExtras(){
        return this.extras;
    }

    public Selects getSelects(){
        return this.selects;
    }

}
