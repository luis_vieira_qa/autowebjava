package core.elements.core;

import core.elements.reader.elementObject.YmlElementAttributes;
import core.elements.reader.elementObject.YmlElementObject;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import parallel.setup.Hooks;
import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


public class Finder{
    protected WebDriver driver = Hooks.driver;
    protected String pageEl;
    protected YmlElementObject elementList;

    private int defaultTimeout = 10;
    private String elDescription;
    private String elName;
    private String elFinder = "";
    private String elLocator = "";
    private By elBy;


    protected WebElement findEl(String elName){
        solveEl(elName);
        return waitElement();
    }

    protected List<WebElement> findList(String elName){
        solveEl(elName);
        return waitElements(elBy);
    }

    protected boolean checkDisplayed(String elName, int timeout){
        solveEl(elName);

        try {
            WebElement element = new WebDriverWait(driver, timeout).until(ExpectedConditions.presenceOfElementLocated(elBy));
            return element.isDisplayed();
        } catch (WebDriverException e){
            System.out.println(e.getMessage());
            return false;
        }
    }

    protected Actions getActions(){
        Actions actions = new Actions(driver);
        return actions;
    }

    protected JavascriptExecutor getJsExecutor(){
        JavascriptExecutor js;
        js = (JavascriptExecutor) driver;
        return js;
    }

    private WebElement waitElement(){
        try {
            return new WebDriverWait(driver, defaultTimeout).until(ExpectedConditions.presenceOfElementLocated(elBy));
        } catch (WebDriverException e){
            assertTrue("Não foi possível encontrar na página: " + pageEl + " o elemento: " + elName + " Seletor do tipo: " + elFinder + " Localizador: " + elLocator + " Descrição: " + elDescription, false);
            return null;
        }
    }

    private List<WebElement> waitElements(By by){
        try {
            return new WebDriverWait(driver, defaultTimeout).until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
        } catch (WebDriverException e){
            assertTrue("Não foi possível encontrar na página: " + pageEl + " um ou mais elementos com o nome: " + elName + " Seletor do tipo: " + elFinder + " Localizador: " + elLocator + " Descrição: " + elDescription, false);
            return null;
        }
    }

    protected WebElement waitClickable(WebElement el){
        try {
            return new WebDriverWait(driver, defaultTimeout).until(ExpectedConditions.elementToBeClickable(el));
        } catch (WebDriverException e){
            assertTrue("O elemento na página: " + pageEl + " com o nome: " + elName + " não é clicável. Seletor do tipo: " + elFinder + " Localizador: " + elLocator + " Descrição: " + elDescription, false);
            return null;
        }
    }

    private void solveEl(String elementName){
        elementList.getElements();

        for(YmlElementAttributes els: elementList.getElements()){
            YmlElementAttributes el = els;
            if(el.getName().equalsIgnoreCase(elementName)){
                elName = el.getName();
                elDescription = el.getDescription();
                elFinder = el.getDesktop().get(0).getFinder();
                elLocator = el.getDesktop().get(0).getLocator();

                break;
            }
        }

        elBy = getBy(elFinder, elLocator);

    }
    protected By getBy(String by, String elLocator){
        switch (by){
            case "css":
                return By.cssSelector(elLocator);
            case "id":
                return By.id(elLocator);
            case "xpath":
                return By.xpath(elLocator);
            default:
                return null;

        }
    }

    protected void emergencySleep(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected int randomizer(int min, int max){
        int random = ThreadLocalRandom.current().nextInt(min, max + 1);
        return random;
    }
}
