package core.elements.reader.elementObject;

import java.util.List;

public class YmlElementAttributes {
    private String name;
    private String description;
    private List<YmlElementDesktop> desktop;
    private List<YmlElementMobile> mobile;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<YmlElementDesktop> getDesktop() {
        return desktop;
    }

    public void setDesktop(List<YmlElementDesktop> desktop) {
        this.desktop = desktop;
    }

    public List<YmlElementMobile> getMobile() {
        return mobile;
    }

    public void setMobile(List<YmlElementMobile> mobile) {
        this.mobile = mobile;
    }
}