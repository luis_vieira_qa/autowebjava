package core.elements.reader.elementObject;

import java.util.List;

public class YmlElementObject {
    public List<YmlElementAttributes> elements;

    public List<YmlElementAttributes> getElements() {
        return elements;
    }

    public void setElements(List<YmlElementAttributes> elements) {
        this.elements = elements;
    }
}
