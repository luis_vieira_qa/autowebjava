package core.elements.reader.elementObject;

public class YmlElementMobile {
    private String finder;
    private String locator;

    public String getFinder() {
        return finder;
    }

    public void setFinder(String finder) {
        this.finder = finder;
    }

    public String getLocator() {
        return locator;
    }

    public void setLocator(String locator) {
        this.locator = locator;
    }
}
