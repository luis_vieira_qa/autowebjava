package core.elements.reader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import core.elements.reader.elementObject.YmlElementObject;

import java.io.File;
import java.io.IOException;

public class ElementReader {

    public YmlElementObject elementObject(String page){
        page = page.toLowerCase();

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        mapper.findAndRegisterModules();
        try {
            page = page.replaceAll("\\.", "/");
            YmlElementObject ymlObject = mapper.readValue(new File("src/test/java/"+page+".yml"), YmlElementObject.class);
            return ymlObject;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
